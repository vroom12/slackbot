package com.vroom.rig.slackbot.message.builder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MessageBuilder {
	
	public static final boolean TAG_IN_SLACK=true;

	public static Map<Integer, List<String>> aliasMap = new HashMap<Integer, List<String>>() {
	    /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
	    	put(1, Arrays.asList(new String[]{"Varun", "<@U141ADKQF>"}));
			put(12, Arrays.asList(new String[]{"Cam", "Frank", "Seti", "<@U18GWR5U6>"}));
			put(3, Arrays.asList(new String[]{"Zack", "Sack", "Elias", "<@U21L709C7>"}));
			put(5, Arrays.asList(new String[]{"Vetter", "Matt", "Vix", "<@U17R4SU0P>"}));
			put(11, Arrays.asList(new String[]{"Atul", "Atool", "A tool", "<@U25QQL563>"}));
			put(2, Arrays.asList(new String[]{"Jack", "Melvin", "Jacky Boy", "<@U190S077E>"}));
			put(8, Arrays.asList(new String[]{"George", "Helding", "The Sausage King", "<@U21L1PX1T>"}));
			put(6, Arrays.asList(new String[]{"Whalen", "Tom", "TWhales", "Tommy", "<@U21KYK7PW>"}));
			put(4, Arrays.asList(new String[]{"Monty", "Minty Jim", "<@U25NGBX6X>"}));
			put(13, Arrays.asList(new String[]{"Soma", "<@U17UH9F0D>"}));
			put(7, Arrays.asList(new String[]{"Bud", "JimBud", "<@U22190ZKQ>"}));
			put(9, Arrays.asList(new String[]{"Zahka", "John", "JZ", "<@U25M6R55G>"}));
	    }
	};
	
	public static Map<Integer, String> slackIdMap = new HashMap<Integer, String>() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			put(1, "<@U141ADKQF>");
			put(12, "<@U18GWR5U6>");
			put(3, "<@U21L709C7>");
			put(5, "<@U17R4SU0P>");
			put(11, "<@U25QQL563>");
			put(2, "<@U190S077E>");
			put(8, "<@U21L1PX1T>");
			put(6, "<@U21KYK7PW>");
			put(4, "<@U25NGBX6X>");
			put(13, "<@U17UH9F0D>");
			put(7, "<@U22190ZKQ>");
			put(9, "<@U25M6R55G>");
		}
	};
}
