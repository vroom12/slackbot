package com.vroom.rig.slackbot.message.builder;

import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;

import com.vroom.rig.slackbot.model.fantasy.BoxPlayer;
import com.vroom.rig.slackbot.model.fantasy.BoxTeam;

public class DogMessageBuilder extends MessageBuilder {
	
	private static final int NUM_MESSAGES = 3;
	private static int lastMessageVal = -1;

	public static String buildDogMessage(BoxPlayer player, BoxTeam team) {
		String message = "";
		Random random = new Random();
		String playerNickname = player.getPlayer().getPlayerDisplays().get(
				random.nextInt(player.getPlayer().getPlayerDisplays().size()));
		String playerName = player.getPlayer().getFirstName() + " " + player.getPlayer().getLastName();
		
		
		String slackId = slackIdMap.get(team.getTeam().getTeamId());
		List<String> aliases = aliasMap.get(team.getTeam().getTeamId());
		
		int next =  random.nextInt(NUM_MESSAGES);
		while(next == lastMessageVal) {
			next =  random.nextInt(NUM_MESSAGES);
		}
		lastMessageVal = next;
		switch (next) {
			case 0:
				message += playerName + " missed his ESPN projection by *" 
					+ (player.getCurrentPeriodProjectedStats().getAppliedStatTotal() - player.getCurrentScore())
					+ "* points. Matthew Berry is really screwing " + aliases.get(random.nextInt(aliases.size())) + "!";
				break;
			case 1:
				message += playerName + " has *" + player.getCurrentScore() + "* points. "
					+ playerNickname + " was supposed to get *" + player.getCurrentPeriodProjectedStats().getAppliedStatTotal() 
					+ "*! Why so low " + aliases.get(random.nextInt(aliases.size())) + "?";
				break;
			case 2:
				message += playerName + " only has *" + player.getCurrentScore() 
					+ "* points but was projected for *" + player.getCurrentPeriodProjectedStats().getAppliedStatTotal()
					+ "* points. What a *DOG*...sucks for " + aliases.get(random.nextInt(aliases.size())) + "!";
				break;
			default: 
				message += "WHOOPS";
				break;
		}
	
		return message;
	}
	
}
